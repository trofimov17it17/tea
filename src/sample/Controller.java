package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;


public class Controller {

    @FXML
    private MenuButton valute;

    @FXML
    private MenuItem rub;

    @FXML
    private MenuItem dollar;

    @FXML
    private MenuItem euro;

    @FXML
    private TextField sum;

    @FXML
    private MenuButton procent;

    @FXML
    private MenuItem procent0;

    @FXML
    private MenuItem procent3;

    @FXML
    private Label MoneyChai;

    @FXML
    private MenuItem procent5;

    @FXML
    private MenuItem procent7;

    @FXML
    private MenuItem procent10;

    @FXML
    private Button result;

    @FXML
    private TextField vivod;

    @FXML
    void actDollar(ActionEvent event) {
        valute.setText("Доллары");
    }

    @FXML
    void actRub(ActionEvent event) {
        valute.setText("Рубли");
    }

    @FXML
    void aсtEuro(ActionEvent event) {
        valute.setText("eвро");
    }

    @FXML
    void actProcent0(ActionEvent event) {
        procent.setText("0");
    }

    @FXML
    void actProcent3(ActionEvent event) {
        procent.setText("3");
    }

    @FXML
    void actProcent5(ActionEvent event) {
        procent.setText("5");
    }

    @FXML
    void actProcent7(ActionEvent event) {
        procent.setText("7");
    }

    @FXML
    void actProcent10(ActionEvent event) {
        procent.setText("10");
    }


    @FXML
    void result(ActionEvent event) {
        double sam = Double.parseDouble(sum.getText());
        double procent1 = Double.parseDouble(procent.getText());
        double result = (sam * procent1) / 100;
        vivod.setText(String.valueOf(result));
    }
}


